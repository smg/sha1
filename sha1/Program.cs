﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace sha1
{
    internal class Program
    {
        private static SHA1CryptoServiceProvider sha = new SHA1CryptoServiceProvider();
        private const string EXIT = "end";

        private static void Main(string[] args)
        {
            Console.WriteLine("SHA1 hash");

            List<string> paths = new List<string>();
            List<byte[]> sha1bytes = new List<byte[]>();

            if (args != null && args.Length > 0)
            {
                ReadFromArgs(args, paths, sha1bytes);
            }
            else
            {
                ReadFromConsole(paths, sha1bytes);
            }

            if (sha1bytes.Count > 1)
            {
                CompareSHA1(paths, sha1bytes);
            }

            Console.ReadKey();
        }

        private static void ReadFromArgs(string[] args, List<string> paths, List<byte[]> sha1bytes)
        {
            foreach (var path in args)
            {
                FillSHA1(paths, sha1bytes, path, true);
            }
        }

        private static void ReadFromConsole(List<string> paths, List<byte[]> sha1bytes)
        {
            string command = "";
            while (command != EXIT)
            {
                Console.WriteLine(string.Format("Enter path to next file or \"{0}\":", EXIT));
                command = Console.ReadLine();
                if (command != EXIT)
                {
                    FillSHA1(paths, sha1bytes, command, false);
                }
            }
        }

        private static void CompareSHA1(List<string> paths, List<byte[]> sha1bytes)
        {
            bool allEqual = true;
            for (int i = 1; i < sha1bytes.Count; i++)
            {
                if (!sha1bytes[i].SequenceEqual(sha1bytes[0]))
                {
                    Console.WriteLine(string.Format("File {0} has other SHA1. Files {1} and {2} not equal.", paths[i], 1, i + 1));
                    allEqual = false;
                }
            }
            if (allEqual)
            {
                Console.WriteLine("All files have same SHA1 (seems to be equal)");
            }
        }

        private static void FillSHA1(List<string> paths, List<byte[]> sha1bytes, string path, bool printPath)
        {
            if (printPath)
            {
                Console.WriteLine(path);
            }
            var sha1 = GetSHA1(path);
            if (sha1 != null)
            {
                paths.Add(path);
                sha1bytes.Add(sha1);
                Console.WriteLine(BitConverter.ToString(sha1));
            }
        }

        private static byte[] GetSHA1(string path)
        {
            byte[] result = null;
            try
            {
                using (var stream = new FileStream(path, FileMode.Open))
                    result = sha.ComputeHash(stream);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return result;
        }
    }
}